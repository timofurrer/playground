## 1.0.3 (2024-03-26)

### Fixed (2 changes)

- [Test change](timofurrer/playground@bc3bb7ad53b49522836a07294818437bdd515c26)
- [Update](timofurrer/playground@77a96ba15f13be717f0041c41cf5abdce4218024)

### Performance (1 change)

- [Test update](timofurrer/playground@0265a2d1cedb95c7cf0f79686711632a10e5b709)

### test (1 change)

- [Test MR](timofurrer/playground@2005993d6406399e2fb9da33cae77ad0159f76fb) ([merge request](timofurrer/playground!3))
