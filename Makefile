build:
	docker buildx build --platform 'linux/amd64' --file Dockerfile --tag "${CI_REGISTRY_IMAGE}/playground:${CI_COMMIT_SHA}" .
	docker push "${CI_REGISTRY_IMAGE}/playground:${CI_COMMIT_SHA}"
	crane digest --full-ref "${CI_REGISTRY_IMAGE}/playground:${CI_COMMIT_SHA}" | xargs cosign sign
