
terraform {
  backend "http" {}

  encryption {
    # method "unencrypted" "gitlab_tofu_auto_encryption_migrate" {}

    key_provider "pbkdf2" "gitlab_tofu_auto_encryption" {
      passphrase = "sdfgsdfg234234234sdfgsfdgsdfgsdfgsdfgsdfgsdgfsdfgsdfg"
    }

    method "aes_gcm" "gitlab_tofu_auto_encryption" {
      keys = key_provider.pbkdf2.gitlab_tofu_auto_encryption
    }

    state {
      method = method.aes_gcm.gitlab_tofu_auto_encryption

      # fallback {
      #   method = method.unencrypted.gitlab_tofu_auto_encryption_migrate
      # }
    }

    plan {
      method = method.aes_gcm.gitlab_tofu_auto_encryption

      # fallback {
      #   method = method.unencrypted.gitlab_tofu_auto_encryption_migrate
      # }
    }

  }
}

resource "local_file" "foo" {
  content  = "foo!"
  filename = "${path.module}/foo.bar"
}

resource "random_password" "password" {
  length  = 16
  special = true
}

output "filename" {
  value = local_file.foo.filename
}

output "password" {
  value     = random_password.password.result
  sensitive = true
}
